package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) { // Loop for each round of the game.
            System.out.println("Let's play round " + roundCounter);
            String human_choice = readInput("Your choice (Rock/Paper/Scissors)?");
            String computer_choice = getCompChoice(rpsChoices);
            System.out.print("Human chose " + human_choice + ", computer chose " + computer_choice + ". ");

            // Checking who won and printing resaults and keeping scores.
            if (checkWinCondition(human_choice, computer_choice)) {
                System.out.println("Human wins!");
                humanScore ++;
            } else if (checkWinCondition(computer_choice, human_choice)) {
                System.out.println("Computer wins!");
                computerScore ++;
            } else {
                System.out.println("It's a tie!");
            }
            System.out.println("Score: human " + humanScore+ ", computer " + computerScore);
            roundCounter ++;

            // Prompt the user if they want to continue.
            String cont = contPlaying();
            if (cont.equals("n")) {
                break;
            }
        }

        // Printing out the goodbye message.
        System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    // getCompChoice gets a random choice from a list (for the computer's choice).
    public String getCompChoice(List<String> rpsChoices2) {
        int rnd = new Random().nextInt(rpsChoices2.size());
        return rpsChoices2.get(rnd);
    }

    // checkWinCondition checks if if one has won or not.
    public boolean checkWinCondition (String choice1, String choice2) {
        if (choice1.equals("paper")) {
            if (choice2.equals("rock")) {
                return true;
            } else return false;
        } else if (choice1.equals("scissors")) {
            if (choice2.equals("paper")) {
                return true;
            } else return false;
        } else {
            if (choice2.equals("scissors")) {
                return true;
            } else return false;
        }
    }

    // contPlaying prompts the user if they want to continue playing and accepts "y" for yes or "n" for no.
    public String contPlaying () {
        while (true) {
            var contA = readInput("Do you wish to continue playing? (y/n)?");
            if (contA.equals("y") || contA.equals("n")) {
                return contA;
            } else {
                System.out.println("I don't understand " + contA + ". Try again");
            }
        }
    }

}
